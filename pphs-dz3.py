import pycuda.autoinit
import pycuda.driver as drv
import numpy as np
from pycuda.compiler import SourceModule
import math

mod = SourceModule("""
#include<math.h>
__global__ void calculate(float *dest, float *src)
{
  __shared__ float cache[32];

  const int idx = blockIdx.x * blockDim.x + threadIdx.x;
  const int tidx = threadIdx.x;
  cache[tidx] = pow(src[idx], 2);
  int active = blockDim.x / 2;

  do
  {
     __syncthreads();
     if (tidx < active)
       {
          cache[tidx] = cache[tidx] + cache[tidx + active];
       }
     active /= 2;
   }
   while (active > 0);

   if (tidx == 0)
     {
        dest[blockIdx.x] = cache[0];
     }
}

__global__ void calculate1(float *dest, float *src1, float *src2)
{
  __shared__ float cache1[16];
  
  const int idx = blockIdx.x * blockDim.x + threadIdx.x;
  const int tidx = threadIdx.x;
  cache1[tidx] = src1[idx] * src2[idx];
  int active = blockDim.x / 2;

  do
    {
      __syncthreads();
      if (tidx < active)
        {
          cache1[tidx] = cache1[tidx] + cache1[tidx + active];
        }
      active /= 2;
    }
  while (active > 0);
  
  if (tidx == 0)
    {
       dest[blockIdx.x] = cache1[0];
    }
}

__global__ void calculate_norm(float *dest, float *src)
{
  __shared__ float cache[16];
  
  const int idx = blockIdx.x * blockDim.x + threadIdx.x;                      
  const int tidx = threadIdx.x;                                                
  cache[tidx] = sqrt(pow(src[idx], 2));                                        
  int active = blockDim.x / 2;
  
  do
  {                                                                            
      __syncthreads();
      
      if (tidx < active)                                                       
        {                                                                      
          cache[tidx] = cache[tidx] + cache[tidx + active];
        }
      active /= 2;
   }
   while (active > 0);
  
  if (tidx == 0)
     {
        dest[blockIdx.x] = cache[0];
     }
}

__global__ void calculate_scalar_product(float *dest, float *src1, float *src2, float *a, float *b)
{
  __shared__ float cache[16];

   const int idx = blockIdx.x * blockDim.x + threadIdx.x;
   const int tidx = threadIdx.x;
   cache[tidx] = sqrt(src1[idx]*src2[idx]*(acos(a[idx]*b[idx]/src1[idx]*src2[idx])));
   int active = blockDim.x / 2;

   do
  {
   __syncthreads();

   if (tidx < active)
    {
        cache[tidx] = cache[tidx] + cache[tidx + active];
    }
    active /= 2;
  }
  while (active > 0);

  if(tidx == 0)
    {
       dest[blockIdx.x] = cache[0];
    }    
}
""")
calculate = mod.get_function("calculate")
calculate1 = mod.get_function("calculate1")

x = np.random.rand(512).astype(np.float32)
y = np.random.rand(512).astype(np.float32)

mid_result_gpu = np.empty(16).astype(np.float32)
result_gpu = np.empty(1).astype(np.float32)

mid_result_gpu1 = np.empty(16).astype(np.float32)
result_gpu1 = np.empty(1).astype(np.float32)

calculate(drv.Out(mid_result_gpu), drv.In(x),
                            block=(32,1,1), grid=(16,1))

calculate(drv.Out(result_gpu), drv.In(mid_result_gpu),
                            block=(16,1,1), grid=(1,1))

result_cpu = np.power(x,2)

calculate1(drv.Out(mid_result_gpu1), drv.In(x), drv.In(y),
                            block=(32,1,1), grid=(16,1))

calculate1(drv.Out(result_gpu1), drv.In(mid_result_gpu1), drv.In(mid_result_gpu1),
                            block=(16,1,1), grid=(1,1))

result_cpu1 = x*y

calculate_norm = mod.get_function("calculate_norm")

a = np.random.rand(2048).astype(np.float32)
b = np.random.rand(2048).astype(np.float32)

mid_result_gpu_norm_a = np.empty(16).astype(np.float32)
result_gpu_norm_a = np.empty(1).astype(np.float32)

calculate_norm(drv.Out(mid_result_gpu_norm_a), drv.In(a),
                              block=(16,16,1), grid=(8,1))

calculate_norm(drv.Out(result_gpu_norm_a), drv.In(mid_result_gpu_norm_a),
                              block=(8,1,1), grid=(1,1))

result_cpu_norm_a = np.sqrt(a*a)

mid_result_gpu_norm_b = np.empty(16).astype(np.float32)
result_gpu_norm_b = np.empty(1).astype(np.float32)

calculate_norm(drv.Out(mid_result_gpu_norm_b), drv.In(b),
                                             block=(16,16,1), grid=(8,1))

calculate_norm(drv.Out(result_gpu_norm_b), drv.In(mid_result_gpu_norm_b),
                                             block=(8,1,1), grid=(1,1))

result_cpu_norm_b = np.sqrt(b*b)

calculate_scalar_product = mod.get_function("calculate_scalar_product")

mid_result_gpu_scalar_product = np.empty(16).astype(np.float32)
result_gpu_scalar_product = np.empty(1).astype(np.float32)

calculate_scalar_product(drv.Out(result_gpu_scalar_product),drv.In(np.float32(result_gpu_norm_a)), drv.In(np.float32(result_gpu_norm_b)), drv.In(a), drv.In(b),
                                   block=(16,16,1), grid=(8,1))

result_cpu_scalar_product = np.arccos(a*b/result_cpu_norm_a * result_cpu_norm_b)

#ispis svih rezultata
print result_cpu
print result_gpu[0]

print result_cpu1
print result_gpu1[0]

print result_cpu_norm_a
print result_gpu_norm_a[0]

print result_cpu_norm_b
print result_gpu_norm_b[0]

print result_cpu_scalar_product
print result_gpu_scalar_product[0]
