import pycuda.autoinit
import pycuda.driver as drv
import pycuda.gpuarray as ga
import numpy as np
from pycuda.compiler import SourceModule

mod = SourceModule("""
#include<stdio.h>
#include<math.h>

__global__ void area(float2 point1, float2 point2, float2 point3, float *dest)
{
 
   dest[0] = 0.5*fabs((point1.x-point3.x)*(point2.y-point1.y)-(point1.x-point2.x)*(point3.y-point1.y));

}

__global__ void perimeter(float2 point1, float2 point2, float2 point3, float *dest)
{

   double a = sqrt(pow((point2.x-point1.x),2) + pow((point2.y-point1.y),2));
   
   double b = sqrt(pow((point3.x-point2.x),2) + pow((point3.y-point2.y),2));
   
   double c = sqrt(pow((point1.x-point3.x),2) + pow((point1.y-point3.y),2));

   dest[0] = a + b + c;

}
                                        
""")

area = mod.get_function("area")

A = ga.vec.make_float2(3.6, 5.4)

B = ga.vec.make_float2(23.1, 51.4)

C = ga.vec.make_float2(1.6, 45.4)

D = ga.vec.make_float2(31.6, 15.4)

result_gpu = np.empty(1).astype(np.float32)

area(A, B, C, drv.Out(result_gpu),
                                  block=(1,1,1), grid=(1,1))
print "Povrsina ABC: ",result_gpu[0]

area(A, B, D, drv.Out(result_gpu),
                                  block=(1,1,1), grid=(1,1))
print "Povrsina ABD: ",result_gpu[0]

area(B, C, D, drv.Out(result_gpu),
                                  block=(1,1,1), grid=(1,1))
print "Povrsina BCD: ",result_gpu[0]


perimeter = mod.get_function("perimeter")

result_gpu_per = np.empty(1).astype(np.float32)

perimeter(A, B, C, drv.Out(result_gpu_per),
                                  block=(1,1,1), grid=(1,1))
print "Opseg ABC: ",result_gpu_per[0]

perimeter(A, B, D, drv.Out(result_gpu_per),
                                  block=(1,1,1), grid=(1,1))
print "Opseg ABD: ",result_gpu_per[0]

perimeter(B, C, D, drv.Out(result_gpu_per),
                                  block=(1,1,1), grid=(1,1))
print "Opseg BCD: ",result_gpu_per[0]
