#!/usr/bin/env python
# -*- coding: utf-8 -*

import pycuda.autoinit
import pycuda.driver as drv
import numpy as np
from pycuda.compiler import SourceModule

mod = SourceModule("""
#include<stdio.h>

__global__ void vector_sum (float *dest, float *a, float *b, float *c)
{
  const int i = threadIdx.x;
   dest[i] = 3*a[i]+4*b[i]-5*c[i];
   printf("Na GPU-u procesna nit %d je izračunala vrijednost %.2f, uzimajući kao ulazne parametre %.2f, %.2f, %.2f.\\n", i,dest[i],*a,*b,*c);
  }
            """)

vector_sum = mod.get_function("vector_sum")

a = np.ones(350, dtype=np.float32)

b = np.ones(350, dtype=np.float32)
b= b*1.5
    
c = np.ones(350, dtype=np.float32)
c = c*2

result_gpu = np.zeros_like(a)

vector_sum(drv.Out(result_gpu), drv.In(a), drv.In(b), drv.In(c),
                                 block=(350,1,1), grid=(1,1))

result_cpu = 3*a+4*b-5*c

print "CPU rezultat\n", result_cpu
print "GPU rezultat\n", result_gpu
print "CPU i GPU daju isti rezultat?\n", (result_cpu == result_gpu).all()
