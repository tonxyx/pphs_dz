# -*- coding: utf-8 -*-                                                         
import pphs_dz5_Butkovic_Toni
import numpy as np
import csv

def test_produkt_matrica():
    file = open('matricaA.txt', 'rb')
    data = csv.reader(file, delimiter='\t')
    a = np.array(list(data)).astype('float32')

    file0 = open('matricaB.txt', 'rb')
    data0 = csv.reader(file0, delimiter='\t')
    b = np.array(list(data0)).astype('float32')

    file1 = open('matricaAB.txt', 'rb')
    data1 = csv.reader(file1, delimiter='\t')
    result_cpu = np.array(list(data1)).astype('float32')

    print result_cpu
    print pphs_dz5_Butkovic_Toni.produkt_matrica(a, b)

    assert (pphs_dz5_Butkovic_Toni.produkt_matrica(a, b) == result_cpu).all()
    print "Test uspješno prolazi."


if __name__ == "__main__":
    test_produkt_matrica()
