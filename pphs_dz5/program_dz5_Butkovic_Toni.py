# -*- coding: utf-8 -*-

import pycuda.autoinit
import pycuda.driver as drv
import pycuda.gpuarray as ga
import numpy as np
import csv

file = open('matricaA.txt', 'rb')
data = csv.reader(file, delimiter='\t')
a = np.array(list(data)).astype('float32')

file0 = open('matricaB.txt', 'rb')
data0 = csv.reader(file0, delimiter='\t')
b = np.array(list(data0)).astype('float32')

print a.shape
print b.shape

result = np.dot(a, b)
print result
np.savetxt('matricaAB.txt', result, delimiter='\t')
