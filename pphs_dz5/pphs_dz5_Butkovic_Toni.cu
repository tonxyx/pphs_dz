__global__ void matrix_mult (float *result, float *mat1, float *mat2)          {                                                                               
  float value = 0;                                                               for (int k = 0; k < 1024; k++)                                                   {                                                                                value += mat1[k * blockDim.x * gridDim.x + threadIdx.x + blockIdx.x * blockDim.x] * mat2[(threadIdx.y + blockIdx.y * blockDim.y) * blockDim.x * gridDim.
x + k];                                                                            }                                                                          

  int idx = (threadIdx.y + blockIdx.y * blockDim.y) * blockDim.x * gridDim.x + 
threadIdx.x + blockIdx.x * blockDim.x;                                         

  result[idx] = value;                                                         }