# -*- coding: utf-8
import pycuda.autoinit
import pycuda.driver as drv
import pycuda.gpuarray as ga
import numpy as np
from pycuda.compiler import SourceModule

mod = SourceModule(open("pphs_dz5_Butkovic_Toni.cu").read())

def produkt_matrica(a, b):
    """
	Množenje dviju matrica.                                                        
    """
    
    assert len(a[0]) == len(b)

    matrix_mult = mod.get_function("matrix_mult")

    result_gpu_mult = np.empty(shape=(len(a), len(b[0])))
    
    matrix_mult(drv.Out(result_gpu_mult), drv.In(a), drv.In(b),
                block=(16,16,1), grid=(1,1))

    return result_gpu_mult
